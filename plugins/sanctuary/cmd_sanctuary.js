'use strict';
/*globals  require, console, module, persist, echo , tusail,sanctuary  */
//default vars and requires

var commando = require('../commando/commando').commando;

//TODO: add permissions checking for each command

/**
 * This command is the main tool for managing, creating and deploying Sanctuaries.
 */
commando('sanctuary', function (params, sender) {
    var command = params[0];
    var sanctuary = Sanctuary.create();
    var creator;
    var blockdata;
    var loc = sender.location;
    loc.add(sanctuary._offset.x, sanctuary._offset.y, sanctuary._offset.z);


    switch (command) {

        // Summon a created sanctuary to the players location
        case 'summon':
            creator = sender;
            //TODO: optionally take a user ID to set as creator
            // creator = params[1]

            echo(sender, 'Placing sanctuary');
            //sanctuary.summon();

            sanctuary = sanctuaryTemplates[sender.name];
            sanctuary.setcreator(sender.name);
            sanctuary.setlocation(loc);
            sanctuary.activate();

            blockdata = sanctuary.getblocks();
            tusail.pasteblocks(sender, loc, blockdata);

            break;

        // Use to create a new sanctuary template. Should only be available to Cruthadaire
        case 'save':
            creator = sender;
            //TODO: optionally take a user ID to set as creator
            // creator = params[1]

            echo(sender, 'Storing this sanctuary...');

            // First, we need to get a copy of the structure of blocks that are intended as the Sanctuary
            blockdata = tusail.getblocks(sender, loc, sanctuary.area.width, sanctuary.area.height, sanctuary.area.depth);

            // Set the schematic for building this structure again
            sanctuary.setblocks(blockdata);

            // Set the creator
            sanctuary.setcreator(sender.name);  //TODO: should be UUID when we are doing this for real

            // Save the sanctuary to file or database
            sanctuary.save();

            break;

        // Remove a summoned sanctuary.
        case 'remove':
            echo(sender, 'Removing Sanctuary');
            //TODO: Create the code for remove
            break;

        // Display all of the help for the sanctuary command.
        case 'help':
            echo(sender, 'Available commands:');
            echo(sender, '  Summon');
            echo(sender, '  Create');
            echo(sender, '  Remove');
            echo(sender, '  Find');
            echo(sender, '  Teleport');
            echo(sender, '  Setblock');
            echo(sender, '  Getblocks');
            echo(sender, '  Pasteblocks');

            break;

        // Find where a summoned sanctuary is in the world
        case 'find':
            echo(sender, 'Finding where that Sanctuary is.');
            //echo(sender, 'What, did you lose it?');
            //TODO: Create the code for find

            break;

        // Teleport to a specific sanctuary or your 'home' sanctuary (if you have one)
        case 'home':
            echo(sender, "Not yet implemented!");
            console.info("Sanctuaty"+sanctuaries[0]);
            break;

        // Join the membership of this sanctuary
        case 'join':
            echo(sender, "Not yet implemented!");
            break;

        // Invite a player to join this sanctuary
        case 'invite':
            echo(sender, "Not yet implemented!");
            break;


        /*********************************************************************************/
        // TEST functions - these WILL be deleted
        case 'setblock':
            echo(sender, "PARAMS: " + params);
            tusail.createblock(sender, loc, params);
        // Teleport to a specific sanctuary or your 'home' sanctuary (if you have one)
        case 'home':
            echo(sender, "Not yet implemented!");
            console.info("Sanctuaty"+sanctuaries[0]);
            break;

        // Join the membership of this sanctuary
        case 'join':
            echo(sender, "Not yet implemented!");
            break;

        // Invite a player to join this sanctuary
        case 'invite':
            echo(sender, "Not yet implemented!");
            break;


        /*********************************************************************************/
        // TEST functions - will be deleted
        case 'createblock':
            var blockId = params[1];
            var metadata = params[2];

            tusail.createblock(sender, loc, blockId, metadata);
            break;


        case 'getblocks':
            blockdata = tusail.getblocks(sender, loc, params);
            break;


        case 'pasteblocks':
            tusail.pasteblocks(sender, loc, params);
            break;


        /*********************************************************************************/
        // Display all of the help for the sanctuary command. Can we include the sanctuary command in the help menu? example: /help sanctuary
        default:
            echo(sender, 'Available commands:');
            echo(sender, '  Summon');
            echo(sender, '  Create');
            echo(sender, '  Remove');
            echo(sender, '  Find');
            echo(sender, '  Teleport');
            echo(sender, '  Setblock');
            echo(sender, '  Getblocks');
            echo(sender, '  Pasteblocks');
            break;

        //TODO: add settings editing commands
    }
});
