'use strict';
/*globals  require, console, module, persist, echo, exports   */

/**
 * https://hub.spigotmc.org/javadocs/bukkit/
 */

/**
 * SIGNS: facing is in 1/16 of clockwise facing, starting at south.
 * so, metadata 0 is a south facing sign
 *
 */

/**
 * STAIRS: facing E=0, W=1, S=2 N=3
 *
 */



var tusail = {

    /**
     * Creating blocks - performs permissions checks and costing on players for creating blocks in game
     * If createblock is called from a console or server script, no costing should be assessed.
     *
     * @param sender
     * @param loc
     * @param params
     * @returns {*}
     */
    createblock: function (sender, loc, blockId, metadata) {
        if (typeof loc == 'undefined') {
            echo(sender, 'You need to specify a location - or be in game.');
            //TODO: exit NOW
        }

        if (typeof blockId == 'undefined') {
            blockId = 1;
        }

        if (typeof metadata == 'undefined') {
            metadata = 0;
        }

        //console.info("RECEIVED: "+sender+", "+loc+", "+blockId+", "+metadata+")");

        //TODO: Do costing

        // this gets the target block to manipulate
        var block = loc.world.getBlockAt(loc.x, loc.y, loc.z);

        // this sets the block data ID
        block.setTypeIdAndData(blockId, metadata, true
        )
        ;

        // this addds any meta data (facing, color, etc)
        block.data = metadata;

        return block;
    },

    getblocks: function(sender, loc, width, height, depth) {
        if (typeof loc == 'undefined') {
            echo(sender, 'You need to specify a location - or be in game.');
            //TODO: exit NOW
        }

        var block;
        var blockdata = [];
        for (var x=0;x<width;x++) {
            for (var y=0; y<height; y++) {
                for (var z=0; z<depth; z++) {
                    block = loc.world.getBlockAt(loc.x+x, loc.y+y, loc.z+z);
                    blockdata.push({
                        type: block.getTypeId(),
                        metadata: block.data,
                        x: x,
                        y: y,
                        z: z
                    });
                }
            }
        }

        return blockdata;
    },

    pasteblocks: function(sender, loc, blockdata) {
        var tgt;

        for (var i=0;i<blockdata.length;i++) {
            tgt = loc;
            tgt.add(blockdata[i].x, blockdata[i].y, blockdata[i].z);
            this.createblock(sender, tgt, blockdata[i].type, blockdata[i].metadata);
        }

    }

};

exports.tusail = tusail;
