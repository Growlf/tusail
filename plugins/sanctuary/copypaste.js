'use strict';
/*globals  require, console, module,persist   */
//default vars and requires
var Drone = require('drone');
var blocks = require('blocks');


var tools = {
    _clipBoard : {},
    loadblock: function (params, name) {
        /*jshint validthis:true */
        var loaded = persist(params[0]);
        this._clipBoard[params[0]] = loaded;
        this.custompaste(params[0]);
    },
    copysaveblock: function (params, name) {
        /*jshint validthis:true */
        this.savecopy(params[0], parseInt(params[1]), parseInt(params[2]), parseInt(params[3]),
            function (data, name, self) {
                persist(name, data, true);
            });
    },
    copypasteblock: function (params, name) {
        /*jshint validthis:true */
        this.savecopy(params[0], parseInt(params[1]), parseInt(params[2]), parseInt(params[3]),
            function (data, name, self) {
                self.right(10);
                this.custompaste(name);
                persist(name, data, true);
            });
    },
    savecopy: function (name, w, h, d, callback) {
        /*jshint validthis:true */
        var ccContent = [];
        this.traverseWidth(w, function (ww) {
            ccContent.push([]);
            this.traverseHeight(h, function (hh) {
                ccContent[ww].push([]);
                this.traverseDepth(d, function (dd) {
                    var b = this.getBlock();
                    ccContent[ww][hh][dd] = {type: b.getTypeId(), data: b.data};
                });
            });
        });
        if (typeof callback === "function") {
            this._clipBoard[name] = {dir: this.dir, blocks: ccContent};
            callback(this._clipBoard[name], name, this);
        }
    },
    custompaste: function (name, immediate) {
        /*jshint validthis:true */
        var Drone = this.constructor;
        var ccContent = this._clipBoard[name];
        if (ccContent === undefined) {
            console.warn('Nothing called ' + name + ' in this._clipBoard!');
            return;
        }
        var srcBlocks = ccContent.blocks;
        var srcDir = ccContent.dir; // direction player was facing when copied.
        var dirOffset = (4 + (this.dir - srcDir ) ) % 4;

        this.traverseWidth(srcBlocks.length, function (ww) {
            var h = srcBlocks[ww].length;
            this.traverseHeight(h, function (hh) {
                var d = srcBlocks[ww][hh].length;
                this.traverseDepth(d, function (dd) {
                    var b = srcBlocks[ww][hh][dd],
                        cb = b.type,
                        md = b.data,
                        newDir,
                        dir,
                        a,
                        c,
                        len;
                    //
                    // need to adjust blocks which face a direction
                    //
                    switch (cb) {
                        //
                        // doors
                        //
                        case 64: // wood
                        case 71: // iron
                        case 427: // spruce door
                        case 428: // birch door
                        case 429: // jungle door
                        case 430: // acacia door
                        case 431: // dark oak door
                            // top half of door doesn't need to change
                            if (md < 8) {
                                md = (md + dirOffset ) % 4;
                            }
                            break;
                        //
                        // stairs
                        //
                        case 53:  // oak
                        case 67:  // cobblestone
                        case 108: // red brick
                        case 109: // stone brick
                        case 114: // nether brick
                        case 128: // sandstone
                        case 134: // spruce
                        case 135: // birch
                        case 136: // junglewood
                        case 156: // quartz
                        case 163: // acacia
                        case 164: // dark oak
                        case 180: // red sandstone
                            dir = md & 0x3;
                            a = Drone.PLAYER_STAIRS_FACING;
                            len = a.length;
                            for (c = 0; c < len; c++) {
                                if (a[c] == dir) {
                                    break;
                                }
                            }
                            c = (c + dirOffset ) % 4;
                            newDir = a[c];
                            md = (md >> 2 << 2 ) + newDir;
                            break;
                        //
                        // signs , ladders etc
                        //
                        case 23: // dispenser
                        case 54: // chest
                        case 61: // furnace
                        case 62: // burning furnace (will not exist in 1.9)
                        case 65: // ladder
                        case 68: // wall sign
                        case 158: // dropper
                            a = Drone.PLAYER_SIGN_FACING;
                            len = a.length;
                            for (c = 0; c < len; c++) {
                                if (a[c] == md) {
                                    break;
                                }
                            }
                            c = (c + dirOffset ) % 4;
                            newDir = a[c];
                            md = newDir;
                            break;
                    }
                    this.setBlock(cb, md);
                });
            });
        });
    }
};

//Drone.extend(tools);
