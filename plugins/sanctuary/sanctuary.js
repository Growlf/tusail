'use strict';
/*globals  require, console, module, persist, echo   */
//default vars and requires

var sanctuaryTemplates = {};
var sanctuaries = {};

var sanctuary = {
    _name: 'Wild Sanctuary',    // the name of this sanctuary
    _creator: 'undefined',      // Type is 'undefined' (unaligned), or a Cruthadair name
    _location: 'undefined',     // World.getSpawnLocation();
    area: {
        width: 7,
        height: 8,
        depth: 7,
    },
    _offset: {
        x: -3.0,
        y: -2.0,
        z: -3.0,
    },
    _caretakers: {},             // members of this community
    _invites: {},
    _blockdata: {},              // the schematic for this Sanctuary
    _active: false,


    /****************************************************************
     * Management and action methods
     *
     * TODO: add in checks and permissions etc to all of these
     */

    /**
     * Creates a new Sanctuary object and returns it
     * @returns {sanctuary}
     *
     * TODO: add the option of passing all of the parameters in the create
     */
    create: function () {
        return this;
    },

    save: function () {
        //TODO: save this to mongodb or file
        sanctuaryTemplates[this._creator] = this;
    },

    summon: function () {
        //TODO: validate all properties first
        //TODO: paste the schematic in and activate
        //TODO: setcreator
        //TODO: activate
    },


    /****************************************************************
     * Public-property methods
     *
     * TODO: add in checks and permissions etc to all of these.
     * Some of these will auto-adjust based on 'who' is executing them
     */

    setname: function (name) {
        this._name = name;
    },

    getname: function () {
        return this._name;
    },

    setcreator: function (creator) {
        this._creator = creator;
    },

    getcreator: function () {
        return this._creator;
    },

    setlocation: function (location) {
        this._location = location;
    },

    getlocation: function () {
        return this._location;
    },

    setblocks: function (blockdata) {
        this._blockdata = blockdata;
    },

    getblocks: function () {
        return this._blockdata;
    },

    activate: function () {
        this._active = true;
    },

    is_activate: function () {
        return this._active;
    },

};

exports.Sanctuary = sanctuary;
exports.sanctuaries = sanctuaries;
exports.sanctuaryTemplates = sanctuaryTemplates;
