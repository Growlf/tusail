/**
 * testpkgs - lists all packages that are loaded into the javascript engine from Java
 *
 * The NetYeti <netyeti@thenetyeti.com> - 2015/12/04
 */


function testpkgs() {
  packs = java.lang.Package.getPackages()
  for (p in packs) {
    print(packs[p]);
  }
}
command( testpkgs );
