# ScriptCraft with MongoDB

Yes, that's right - you CAN have your Spigot backed by a MongoDB instance.  This allows interopability with other apps (such as Meteor, and more!) 
 and super simple and extremely powerful persistence of data.

## Running Spigot with additional libraries

To get your spigot server to actually enable the necessary libraries at runtime, download all seven jars listed below and place them into your server's ```\plugins\``` folder.
Then change your run-script to look something more like the following:

```
INVOCATION="java -Xmx$MAXMEM -Xms$INITMEM -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=$CPU_COUNT -XX:+AggressiveOpts -cp mongodb_driver.jar:mongodb_driver_core.jar:bson.jar:jongo.jar:jackson_core.jar:jackson_databind.jar:jackson_annotations.jar:bson4jackson.jar:spigot.jar org.bukkit.craftbukkit.Main nogui"
```

## Java Libraries needed

### JOngo 

These need to be the specified versions.

* http://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-annotations/2.4.1/
* http://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-core/2.4.1/
* http://repo1.maven.org/maven2/com/fasterxml/jackson/core/jackson-databind/2.4.1/
* https://repo1.maven.org/maven2/de/undercouch/bson4jackson/2.4.0/

### Mongo

Use the latest of each of these.

* http://central.maven.org/maven2/org/mongodb/mongodb-driver/
* http://central.maven.org/maven2/org/mongodb/mongodb-driver-core/
* http://central.maven.org/maven2/org/mongodb/bson/

### Links

* MONGO DOCS: http://api.mongodb.org/java/current/com/mongodb/DB.html
* JONGO: http://jongo.org/

Jongo relies upon: Jackson 2.4.1, Bson4Jackson 2.4.0 and Mongo Java Driver 2.11+. Its Maven dependency, an OSGI compliant jar, comes with the first two, you have to provide a dependency to the driver.

# General Reference

* https://hub.spigotmc.org/javadocs/bukkit/
* https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html

Feel free to contact us at https://yeticraft.slack.com/messages/jscripting and ask questions.  If you want to join the project, let us know!

