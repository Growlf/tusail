/**
 * Mongo database connector
 * 2015/12/04
 *
 * Christopher Churchill <vimes1984@gmail.com>
 * Carisus
 * The NetYeti <netyeti@thenetyeti.com>
 *
 * Mongo helper functions so as to abstract the java from the JS
 * Creates a MongoDB connection for use in the Minecraft server.  Store things,
 * run things - fly, be free!  The world is your oyster.
 *
 */


/**

 Prototyping thoughts:

 * set server address (default: localhost)
 * set server port (default: 27017)
 * set database name (yeticraft)
 * set collection name (default: none)

 * mdb (the database connection)
 * check connection to server or error out with message to console
 * check or create databse specified
 * check or create collection specified in database
 * set mdb.connection varable to connected mongoclient.getDB

 * get data (collection, query)
 * check connection
 * try/catch the query
 * return the query object as toArray


 */

var mongodb       = Packages.com.mongodb;
var MongoClient   = mongodb.MongoClient;
var DBObject      = Packages.com.mongodb.DBObject;
var JSONPARSE     = Packages.com.mongodb.util.JSON;
var BasicDBObject = Packages.com.mongodb.BasicDBObject;
var Jongo         = Packages.org.jongo.Jongo;
var RawResultHandler = Packages.org.jongo.RawResultHandler;
var MongoJSON        = Packages.com.mongodb.util.JSON.serialize;
/**
 *
 * @type {{connect: mdb.connect, helpers: {objinsert: mdb.helpers.objinsert, objdelete: mdb.helpers.objdelete}}}
 *
 * var mongo_connection = mdb.connect({server: localhost, port: 27107, database: yeticraft});
 * var collection       = mongo_connection.get({collection: 'testcoll', query: {test:'yay'} });
 */
var mdb = {
    _connection: 'undefined',
    _serveraddress: '127.0.0.1',
    _serverport: '27017',
    _database: 'test',
    _authuser: 'undefined',
    _authpasswd: 'undefined',
    _collectionname: 'undefined',
    _collectionobject: 'undefined',
    _query: 'undefined',
    _jongo: 'undefined',
    _cursor: 'undefined',

    connect: function (ConnectionObj) {
        /**
         * ConnectionObj = {
         *  serveradress: 'localhost',
         *  serverport: 27017,
         *  database: test
         * }
         */

        /*
        if (!this._connection) {
            if (typeof ConnectionObj.serveradress === 'undefined') {
                ConnectionObj.serveradress = 'localhost';
            }
            if (typeof ConnectionObj.serverport === 'undefined') {
                ConnectionObj.serverport = 27107;
            }
            if (typeof ConnectionObj.database === 'undefined') {
                ConnectionObj.database = 'test';
            }
        }
        */


        this._connection = new MongoClient(this._serveraddress, parseInt(this._serverport));
        this._database = this._connection.getDB("test"); //see http://api.mongodb.org/java/current/com/mongodb/DB.html
        this._jongo = new Jongo(this._database); //see 
        return this;
    },
    get: function (CollectionObj) {
        if (typeof CollectionObj.collection === 'undefined') {
            return console.error('Collection is undefined');
        }
        if (typeof CollectionObj.query === 'undefined') {
            CollectionObj.query = "{}";
        }
        this._collectionname       = CollectionObj.collection;
        this._query                = CollectionObj.query;
        this._collectionobject     = this._jongo.getCollection(this._collectionname).find(this._query);//see https://docs.mongodb.org/manual/reference/method/db.createCollection/
        this._cursor               = this._jongo.getCollection(this._collectionname).find(this._query).map(new RawResultHandler());
       return JSON.parse(MongoJSON(this._cursor)); // returns a string so we parse out the escpae caracters :D
    },
    put: function (CollectionObj) {
        if (this._collection === 'undefined') {
            this.get(CollectionObj);
        }

        this._collection.insert(CollectionObj.query);
    },
    update: function(CollectionObj){
          /**
            * EXAMPLE: test.update({collection: "testcoll", doctoupdate: "{test: 'yay'}",  updateobj: "{test: 'BOM'}" });
            *
            */
      if(typeof CollectionObj === 'undefined'){
        return console.warn('you need to at least pass me a object kinda like: DB.update({collection: "CollectionName", doctoupdate: "DOC TO UPDATE", obj: "{UPDATE OBJECT HERE}" } ) ');
      }
      //check if the collection obj is defined if not use our one stored in the function
      if(typeof CollectionObj.collection === 'undefined'){
        if(typeof this._collectionname === 'undefined'){
          //no collection found in memory lets exit
          return console.warn('No collection found');
        }
        CollectionObj.Collection = this._collectionname;
      }else{
        this._collectionname = CollectionObj.collection;
      }
      if(typeof CollectionObj.doctoupdate === 'undefined'){
        return console.warn('nothing to update try setting a object/doc to update like so: DB.update({collection: "CollectionName", doctoupdate: "DOC TO UPDATE", obj: "{UPDATE OBJECT HERE}" } )');
      }
      if(typeof CollectionObj.updateobj === undefined){
         return console.warn('nothing to update try setting a object/doc to update like so: DB.update({collection: "CollectionName", doctoupdate: "{DOC TO UPDATE}",  updateobj: "{UPDATE OBJECT HERE}" } )');
      }

      return this._jongo.getCollection(this._collectionname).update(CollectionObj.doctoupdate).with("{$set: "+ CollectionObj.updateobj +"}"); // returns a string so we parse out the escpae caracters :D
    },
    close: function(){
        this._connection.close();
    },

    helpers: {
        objinsert: function (ObjInsert) {
        },
        objdelete: function (ObjDelete) {
        },
    }
};

module.exports = mdb;
